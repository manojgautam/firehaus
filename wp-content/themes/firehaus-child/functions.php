<?php
function my_theme_enqueue_styles() {

    $parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Logo Custom functions End
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function Copying_customize_register( $wp_customize ) {
  $wp_customize->add_setting('Logo', array());
		 
  //******************************************************
  // General Email Detail
  //******************************************************
   
   $wp_customize->add_control( new WP_Customize_copy_Section_Control( 
    $wp_customize, 'Copy_write', 
    array(
      'label' => __( 'Logo  Section', 'tech' ),
      'section' => 'Logo',
      'settings' => 'Logo',
    ) 
  ));
     $wp_customize->add_setting('LogoImage', array());
  $wp_customize->add_control(new WP_Customize_Image_Control(
    $wp_customize,'LogoImage',
             array(
                 'label'       => __( 'Logo', 'tech' ),
                 'section'     => 'Logo',
                 'settings'    => 'LogoImage',
                 'context'     => 'LogoImage',
             )
         ));
  $wp_customize->add_setting('LogoMobileImage', array());
  $wp_customize->add_control(new WP_Customize_Image_Control(
    $wp_customize,'LogoMobileImage',
             array(
                 'label'       => __( 'Mobile Logo', 'tech' ),
                 'section'     => 'Logo',
                 'settings'    => 'LogoMobileImage',
                 'context'     => 'LogoMobileImage',
             )
         ));



  
     $wp_customize->add_section('Logo' , array(
      'title' => __('Logo','tech'),
  ));
}
add_action( 'customize_register', 'Copying_customize_register' );

if( class_exists( 'WP_Customize_Control' ) ):

class WP_Customize_copy_Section_Control extends WP_Customize_Control { 
    public function render_content() {
    ?>
      <hr>
      <h1><?php echo esc_html( $this->label ); ?></h1>
    <?php
    }
  }
  endif;



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Logo Custom functions End
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

remove_action('load-update-core.php','wp_update_plugins');
add_filter('pre_site_transient_update_plugins','__return_null');