<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'firehaus_db');

/** MySQL database username */
define('DB_USER', 'wordpress_agile');

/** MySQL database password */
define('DB_PASSWORD', 'xwMX5DZ6pudm');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HftxH^/ uM{EY$QK[~0v 6eQu~BZCL`]Vh+0NNDHv#; ,0qAEm~0 rUVra1<v`9s');
define('SECURE_AUTH_KEY',  'g$ON8hObE|3OlC1AvHCtI)!a~Bv%h!:]4??Lh%N`ErcOGIMpG28roWO-}**PuiD)');
define('LOGGED_IN_KEY',    'PI|5wW0~^(??#tqoM@=!TL@+[4j<(]D3in;E*B$gN_f2piKW3%tVNy-M3|DG4 v5');
define('NONCE_KEY',        'VVX1!${J1_a9xI|I3e.,n)fnYCuCflP!f#w>Re6RA8#`%>aAfkQ=Xs~-{1p]bh`/');
define('AUTH_SALT',        ']NZ:KQ}_}^fENVO9Gw|8HjDL&HKe{BOM%Dt%wpAKnaS^_V5xw/4t)]^|f6##[D-N');
define('SECURE_AUTH_SALT', ' x>`%oPo5`I%/x=OqHv**rzW~h/l-Xc@0k_nw_bba*}9lXayd.3:W`(Cflv8m|Gp');
define('LOGGED_IN_SALT',   ';ya@#_UE4W^#Z8Tm[YP/FU{H;jaEX#$E_-j5f^CR9}q4)7$T>5^GHRCz(_GZuvr@');
define('NONCE_SALT',       'ZmQ@Tp-uGK}<?Afb7tzCJA@4e;zb*UC#eL3oZ ^H<n+,=ww<@*/WBnAk~(:`v1gG');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'fh_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
